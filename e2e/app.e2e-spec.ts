import { MySocialPage } from './app.po';

describe('my-social App', () => {
  let page: MySocialPage;

  beforeEach(() => {
    page = new MySocialPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
